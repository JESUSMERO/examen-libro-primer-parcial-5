// expresones regulares validar libros, codigo, titulo, autor, fecha de publicacion y fecha de ingreso
const expresionesRegulares = {
    codigo: /^[a-zA-Z0-9]{5}$/,
    titulo: /^[a-zA-Z0-9]{100}$/,
    autor: /^[a-zA-Z0-9]{60}$/,
    editorial: /^[a-zA-Z0-9]{30}$/,
    fechaPublicacion: /^\d{4}-\d{2}-\d{2}$/,
    fechaIngreso: /^\d{4}-\d{2}-\d{2}$/
};
// Validamos nuestros campos
function validarLibro(codigo, titulo, autor, editorial, fechaPublicacion, fechaIngreso) {
    let valido = true;
    let mensaje = "";
    if (!expresionesRegulares.codigo.test(codigo)) {
        valido = false;
        mensaje = "El codigo debe ser alfanumerico de 5 caracteres";
    }
    if (!expresionesRegulares.titulo.test(titulo)) {
        valido = false;
        mensaje = "El titulo debe ser alfanumerico de 100 caracteres";
    }
    if (!expresionesRegulares.autor.test(autor)) {
        valido = false;
        mensaje = "El autor debe ser alfanumerico de 60 caracteres";
    }
    if (!expresionesRegulares.editorial.test(editorial)) {
        valido = false;
        mensaje = "La editorial debe ser alfanumerico de 30 caracteres";
    }
    if (!expresionesRegulares.fechaPublicacion.test(fechaPublicacion)) {
        valido = false;
        mensaje = "La fecha de publicacion debe ser tipo date";
    }
    if (!expresionesRegulares.fechaIngreso.test(fechaIngreso)) {
        valido = false;
        mensaje = "La fecha de ingreso debe ser tipo date";
    }
    if (valido) {
        alert("Libro ingresado correctamente");
    } else {
        alert(mensaje);
    }
}